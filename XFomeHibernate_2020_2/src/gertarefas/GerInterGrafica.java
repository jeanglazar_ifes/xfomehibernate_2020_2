package gertarefas;

import dominio.Cliente;
import dominio.Pedido;
import intergraf.DlgCadCliente;
import intergraf.DlgCadPedido;
import intergraf.DlgFerramentas;
import intergraf.DlgPesqCliente;
import intergraf.DlgPesqPedido;
import intergraf.DlgRelGroupBy;
import intergraf.FrmPrincipal;
import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;

/**
 *
 * @author jean_
 */
public class GerInterGrafica {
    private FrmPrincipal frmPrinc = null;
    private DlgCadCliente janCadCli = null;
    private DlgPesqCliente janPesqCli = null;
    private DlgCadPedido janCadPed = null;
    private DlgPesqPedido janPesqPed = null;
    private DlgFerramentas janFer = null;
    private DlgRelGroupBy janRelGroupBy;
    
    private GerenciadorDominio gerDom;
    private GerenciadorRelatorios gerRel;
    
    public GerInterGrafica()  {

        try {
            gerDom = new GerenciadorDominio();
            gerRel = new GerenciadorRelatorios();
        } catch (ClassNotFoundException | HibernateException ex ) {
            JOptionPane.showMessageDialog(frmPrinc, ex, "Gerenciador de Interface Gráfica", JOptionPane.ERROR_MESSAGE );
            System.exit(-1);
        }

    }

    public GerenciadorDominio getGerDominio() {
        return gerDom;
    }
    
    public GerenciadorRelatorios getGerRelatorios() {
        return gerRel;
    }
    
    private JDialog abrirJanela(java.awt.Frame parent, JDialog dlg, Class classe){
        if (dlg == null){     
            try {
                dlg = (JDialog) classe.getConstructor(Frame.class, boolean.class, GerInterGrafica.class ).newInstance(parent,true,this);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                JOptionPane.showMessageDialog(frmPrinc, "Erro ao abrir a janela " + classe.getName(), "Gerenciador de Interface Gráfica", JOptionPane.ERROR_MESSAGE );
            }
        }        
        dlg.setVisible(true);  
        return dlg;
    }
    
    public void abrirJanPrincipal(){
        if ( frmPrinc == null) {
            frmPrinc = new FrmPrincipal(this);
        }
        frmPrinc.setVisible(true);
        
    }
    
    public void abrirJanCadCliente(){
        abrirJanela(frmPrinc, janCadCli, DlgCadCliente.class);
    }
    
    public void abrirJanCadPedidos(){
        abrirJanela(frmPrinc, janCadPed, DlgCadPedido.class);
    }
    
    public void abrirJanFerramentas(){
        abrirJanela(frmPrinc, janFer, DlgFerramentas.class);
    }
    
    public Cliente abrirJanPesqCliente(){
        janPesqCli = (DlgPesqCliente) abrirJanela(frmPrinc, janPesqCli, DlgPesqCliente.class);
        return janPesqCli.getCliSelecionado();
    }

    public Pedido abrirJanPesqPedido(){
        janPesqPed = (DlgPesqPedido) abrirJanela(frmPrinc, janPesqPed, DlgPesqPedido.class);
        return janPesqPed.getPedidoSelecionado();
    }
    
    public void abrirJanRelGroupBy() {
        abrirJanela(frmPrinc, janRelGroupBy, DlgRelGroupBy.class);
    }
        
        
    // FUNÇÃO GENÉRICA
    public void carregarCombo( JComboBox combo, Class classe ) {
        List lista;
        try {
            lista = gerDom.listar(classe);
            combo.setModel( new DefaultComboBoxModel( lista.toArray() ) );
        } catch (HibernateException ex) {
            JOptionPane.showMessageDialog(frmPrinc, "Erro ao carregar COMBO. " + ex );
        }

          
    }
    
/*    
    public void carregarComboCidades( JComboBox combo){
        List<Cidade> lista;
        try {
            lista = gerDom.listar(Cidade.class);
            combo.setModel( new DefaultComboBoxModel ( lista.toArray() )  );
        } catch ( HibernateException ex) {
            JOptionPane.showMessageDialog(frmPrinc, "Erro ao carregar cidades. " + ex, "Gerenciador de Interface Gráfica", JOptionPane.ERROR_MESSAGE );
        }
    }
    */
    
    public void sairSistema(){
        System.exit(0);
    }
    
       /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        GerInterGrafica gerIG = new GerInterGrafica();
        gerIG.abrirJanPrincipal();
    }
}
