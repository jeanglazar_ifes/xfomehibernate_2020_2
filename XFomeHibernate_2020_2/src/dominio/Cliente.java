
package dominio;

import gertarefas.FuncoesUteis;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.swing.ImageIcon;

/**
 *
 * @author 1547816
 */        

@Entity
public class Cliente implements Serializable{
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int idCliente;
    
    @Column ( name="nomeCliente", length = 200 )
    private String nome;
    
    @Column ( updatable = false, length = 14, nullable = false)
    private String cpf;
    
    @Column ( updatable = false )
    @Temporal ( TemporalType.DATE )
    private Date dtNasc;   
    
    @Column (length = 1)
    private char sexo;   
    
    private String telFixo;
    private String celular;    
    private String email;
    
    @Lob
    private byte[] foto;
    
    @OneToOne (fetch = FetchType.EAGER,
               cascade = CascadeType.ALL )
    @JoinColumn (name = "idCliente")
    private Endereco endereco;
    
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn (name = "idCidade")
    private Cidade cidade;
    
    @OneToMany (mappedBy = "cliente", fetch = FetchType.LAZY )
    private List<Pedido> pedidos;

    
    // Construtor VAZIO para o Hibernate
    public Cliente() {
    }

    
    public Cliente(String nome, String cpf, Date dtNasc, char sexo, String cep, String ender, int numero, String complemento, String bairro, String referencia, String telFixo, String celular, String email, byte[] foto, Cidade cidade) {
        this.nome = nome;
        this.cpf = cpf;
        this.dtNasc = dtNasc;
        this.sexo = sexo;
        this.telFixo = telFixo;
        this.celular = celular;
        this.email = email;
        this.cidade = cidade;
        this.foto = foto;
        this.endereco = new Endereco(cep, bairro, ender, numero, complemento, referencia);
        this.endereco.setCliente(this);
        
    } 

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
       
    public byte[] getFoto() {
        return foto;
    }

    public ImageIcon getFotoImage() {
        if ( foto != null )
            return new ImageIcon( foto );
        else
            return null;
    }
    
    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDtNasc() {
        return dtNasc;
    }
    
    public String getDtNascFormatada() throws ParseException  {
        return FuncoesUteis.dateToStr(dtNasc);
    }
    
    public String getDtNascBanco() throws ParseException  {
        return FuncoesUteis.dateToStrBanco(dtNasc);
    }
       
    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    
    public String getTelFixo() {
        return telFixo;
    }

    public void setTelFixo(String telFixo) {
        this.telFixo = telFixo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }


    @Override
    public String toString() {
        return nome;
    }
 
    public Object[] toArray() throws ParseException {
        return new Object[] {this, endereco.getBairro(), cidade, getDtNascFormatada(), celular, getFotoImage()};
    }


}
