

package dao;

import dominio.Cliente;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


/**
 *
 * @author 1547816
 */
public class ClienteDAO extends GenericDAO {
     
       
    private List<Cliente> pesquisar ( String pesq, int tipo) throws HibernateException {
        
        Session sessao = null;
        List lista = null;

        try   {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.getTransaction().begin();

            Criteria consulta = sessao.createCriteria(Cliente.class);
            switch (tipo) {
                case 1: consulta.add( Restrictions.like("nome", pesq+"%") );
                        break;
                case 2: consulta.createAlias("endereco", "ender");
                        consulta.add ( Restrictions.like("ender.bairro", pesq+"%") );
                        break;
                case 3: consulta.add( Restrictions.sqlRestriction("MONTH(dtNasc) = " + pesq) );
            }                           
            lista = consulta.list();

            sessao.getTransaction().commit();
            sessao.close();
        } catch ( HibernateException ex) {
            if ( sessao != null) {
                sessao.getTransaction().rollback();
                sessao.close();
            }
            throw new HibernateException(ex);
        }

        return lista;                             
    }
    
    public List<Cliente> pesquisarPorNome ( String pesq ) throws HibernateException {
        return pesquisar(pesq,1);
    }
    
    public List<Cliente> pesquisarPorBairro ( String pesq ) throws HibernateException {
        return pesquisar(pesq,2);
    }
    
    public List<Cliente> pesquisarPorMes ( String pesq ) throws HibernateException {
        return pesquisar(pesq,3);
    }
    
    public List contPorBairro() throws SQLException, Exception {
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            // CRITERIA
            Criteria cons = sessao.createCriteria(Cliente.class);
            cons.createAlias("endereco", "ender");

            // Definir o PROJECTION
            cons.setProjection( Projections.projectionList()
                    .add( Projections.count("idCliente") )
                    .add( Projections.groupProperty("ender.bairro"))
            );

            lista = cons.list();

            sessao.getTransaction().commit();
            sessao.close();
        } catch ( HibernateException ex ) {
            if ( sessao != null) {
                sessao.getTransaction().rollback();
                sessao.close();
            }
            
            throw new HibernateException(ex);
        }
        return lista; 

    }
}
